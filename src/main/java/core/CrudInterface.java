package core;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public interface CrudInterface<T> {

	@Transactional	
	public void save(Object obj);
	
	@Transactional
	public void update(Object obj);
	
	@Transactional
	public boolean delete(Object t);
	
	
	@Transactional
	public void persistense(Object obj);
}
