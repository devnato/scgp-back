package core;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.util.List;
import java.util.Set;

import javax.persistence.Id;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.metadata.ClassMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


@Transactional
public abstract class DataDao<T> implements CrudInterface<T>, Serializable {

	protected Logger logger = Logger.getLogger(getClass());

	@Autowired
	protected SessionFactory sessionFactory;

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	
	@Transactional	
	public synchronized void justSave(Object obj) {
		try {
			getSession().save(obj);
		} catch (Exception e) {
			logger.error("Save erro", e);
		}
	}
	
	@Transactional
	@Override
	public synchronized void save(Object obj) {
		try {
			getSession().saveOrUpdate(obj);
		} catch (Exception e) {
			logger.error("Save erro", e);
		}
	}

	@Transactional
	public void persistense(Object obj) {
		try {

			persist(obj);
		} catch (Exception e) {
			logger.error("Persist erro", e);

		}
	}

	@Transactional
	public void save(List<Object> list) {

		for (Object t : list) {
			save(t);
		}
	}

	@Transactional
	@Override
	public void update(Object obj) {
		try {
			getSession().update(obj);
		} catch (Exception e) {
			logger.error("Update Erro", e);
		}
	}

	@Transactional
	public void update(List<T> list) {
		for (T t : list) {
			update(t);
		}
	}

	public T findById(Class<T> classe, Object id) {

		CriteriaBuilder cb = getSession().getCriteriaBuilder();
		CriteriaQuery<T> q = cb.createQuery(classe);
		Root<T> o = q.from(classe);
		ClassMetadata meta = sessionFactory.getClassMetadata(classe);
		q.where(cb.equal(o.get(meta.getIdentifierPropertyName()), id));

		return getSession().find(classe, id);
	}

	@Transactional
	public T load(Class<T> classe, T t) {
		return getSession().get(classe, getSession().getIdentifier(t));
	}

	protected CriteriaQuery<T> getQuery(Class<T> classe) {
		return getCriteriaBuilder().createQuery(classe);
	}

	@Override
	public boolean delete(Object t) {
		getSession().delete(t);

		return true;
	}

	public List<T> findAll(Class<T> classe) {
		CriteriaQuery<T> query = getQuery(classe);
		query.select(query.from(classe));
		return getSession().createQuery(query).list();
	}

	public void persist(Object t) {
		getSession().persist(t);
	}

	public void persist(List<T> list) {
		for (T t : list) {
			persist(t);
		}
	}

	@Transactional
	public void save(Set<T> list) {
		for (T t : list) {
			getSession().save(t);
		}
	}

	public void merge(Object obj) {
		getSession().merge(obj);
	}

	protected CriteriaBuilder getCriteriaBuilder() {
		return getSession().getCriteriaBuilder();
	}

	public boolean isConnected() {
		return getSession().isConnected();
	}

	public boolean isOpen() {
		return getSession().isOpen();
	}


	

}
