package core;

import java.io.IOException;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Synchronize;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;


public abstract class DataRest {

	protected Logger log = Logger.getLogger(getClass());

	
	protected ResponseEntity<?> getResponse(Object user, HttpStatus status) {
		ResponseEntity<?> response = new ResponseEntity<Object>(user, status);

		return response;
	}

	protected ResponseEntity<?> getResponse(Object user,Throwable t, HttpStatus status) {
		ResponseEntity<?> response = new ResponseEntity<Object>(user, status);

		return response;
	}
	

	protected ResponseEntity<?> getResponse(HttpStatus status) {
		ResponseEntity response = new ResponseEntity(status);

		return response;
	}
	
	
}
