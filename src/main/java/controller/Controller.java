package controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import dao.ProdutoDao;
import domain.Produto;

@org.springframework.stereotype.Controller
public class Controller {

	@Inject
	ProdutoDao produtoDao;	
	
	@RequestMapping(value = "listar", method = RequestMethod.GET, produces={MediaType.APPLICATION_JSON_UTF8_VALUE})
	public ResponseEntity<?> get2() {		
		return new ResponseEntity(produtoDao.findAll(Produto.class),HttpStatus.OK);
	}
	
	@RequestMapping(value = "test", method = RequestMethod.GET)
	public @ResponseBody String get(Object model) {	
		return "test";
	}
	
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public ResponseEntity<?> save(@RequestBody Produto produto) {
		System.out.println("imprimindo o produto"+produto.getValor_compra());
		produtoDao.save(produto);
		return new ResponseEntity(produto,HttpStatus.OK);
	}
	
	@RequestMapping(value = "atualizar", method = RequestMethod.POST)
	public ResponseEntity<?> atualizar(@RequestBody Produto p) {
		
		produtoDao.save(p);
		return new ResponseEntity(p,HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> delete(@PathVariable("id") Long codigo) {		
		Produto p = new Produto();
		p.setCodigo(codigo.intValue());
		produtoDao.delete(p);
		return new ResponseEntity(HttpStatus.OK);
	}
}
